#include <iostream>
#include <cmath>

using namespace std;

class CalcFunc
{
public:
    CalcFunc(double a, double b) {this->a=a; this->b=b;}

    double func(double x, double y)
    {
        return a*x-b*y;
    }

    void RungeKuttExplicit(double* y, double t, double step)
    {
        double k1, k2, k3, k4;
        k1=func(t,*y);
        k2=func(t+step/2, *y+step*k1/2);
        k3=func(t+step/2, *y+step*k2/2);
        k4=func(t+step, *y+step*k3);
        *y += (k1+2*k2+2*k3+k4)*step/6;
    }

    void RungeKuttImplicit(double* y, double t, double step)
    {
        double y1=*y;
        y1+=func(y1, t)*step;
        *y += step*(func(t,*y)+func(t+step,y1))/2;
    }

private:
    double a=1;
    double b=1;
};
int main()
{
    // Вариант 11
    double a=1.2;
    double b=1.3;
    double d=0.5;
    double c=d+(a/pow(b,2));
    double u;
    double y_rungekuttexplicit=d;
    double y_rungekuttimplicit=d;
    double x0=0;
    double step=0.01;
    double xn=1;
    CalcFunc calcFunc(a,b);
    double maxdelta_rungekuttexplicit=0;
    double maxdelta_rungekuttimplicit=0;

    cout<<"x      u          y_rungekuttexplicit   y_rungekuttimplicit"<<endl;
    for (double i=x0; i<=xn+step; i+=step)
    {
        u=(a/b)*(i-1/b)+c*exp(-b*i);
        calcFunc.RungeKuttExplicit(&y_rungekuttexplicit,i,step);
        calcFunc.RungeKuttImplicit(&y_rungekuttimplicit,i,step);
        if (abs(u-y_rungekuttexplicit)>maxdelta_rungekuttexplicit) maxdelta_rungekuttexplicit=abs(u-y_rungekuttexplicit);
        if (abs(u-y_rungekuttimplicit)>maxdelta_rungekuttimplicit) maxdelta_rungekuttimplicit=abs(u-y_rungekuttimplicit);
        cout<<i<<"   "<<u<<"   "<<y_rungekuttexplicit<<"              "<<y_rungekuttimplicit<<endl;
    }
    cout<<"maxdelta_rungekuttexplicit= "<<maxdelta_rungekuttexplicit<<endl;
    cout<<"maxdelta_rungekuttimplicit= "<<maxdelta_rungekuttimplicit<<endl;
    return 0;
}
